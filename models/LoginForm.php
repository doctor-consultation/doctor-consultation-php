<?php

namespace app\models;

use app\core\Application;
use app\core\Model;
use app\models\User;

class LoginForm extends Model
{

    public string $email = '';
    public string $password = '';

    public function rules(): array
    {
        return [
            'email' => [self::RULE_REQUIRED, self::RULE_EMAIL],
            'password' => [self::RULE_REQUIRED]
        ];
    }

    public function labels(): array
    {
        return [
            'email' => 'Your Email',
            'password' => 'Password'
        ];
    }

    public function login()
    {
        $user = User::findOne(['email' => $this->email]);
        if (!$user) {
            $this->addError('email', 'User does not exist with this email');
            return false;
        }
        if (password_verify($this->password, $user->password)) {
            $this->addError('password', 'password is incorrects');
            return false;
        }

        return Application::$app->login($user);
    }

    public function attributes(): array
    {
        return [
            'email' => 'Email',
            'password' => 'Password'
        ];
    }
}
