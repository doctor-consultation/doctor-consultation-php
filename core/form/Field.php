<?php

namespace app\core\form;

use app\core\Model;

class Field
{
    public const TYPE_TEXT = 'text';
    public const TYPE_PASSWORD = 'password';
    public const TYPE_NUMBER = 'number';
    public const TYPE_EMAIL = 'email';

    public string $inputType;
    public Model $model;
    public  string $attribute;

    public function __construct(Model $model, string $attribute)
    {
        $this->inputType = self::TYPE_TEXT;
        $this->model = $model;
        $this->attribute = $attribute;
    }

    public function __toString()
    {
        return sprintf(
            '
        <div class="form-group">
            <label>%s</label>
            <input type="%s" name="%s" value="%s" class="form-control %s">
            <div class="invalid-feedback">%s</div>
        </div>',
            $this->model->getLabel($this->attribute),
            $this->inputType,
            $this->attribute,
            $this->model->{$this->attribute},
            $this->model->hasError($this->attribute) ? 'is-invalid' : '',
            $this->model->getFirstError($this->attribute)
        );
    }

    public function passwordField()
    {
        $this->inputType = self::TYPE_PASSWORD;
        return $this;
    }

    public function emailField()
    {
        $this->inputType = self::TYPE_EMAIL;
        return $this;
    }
}
