<?php

namespace app\core;

class Response
{
    public function setResponseCode(int $responseCode)
    {
        http_response_code($responseCode);
    }

    public function redirect(string $url)
    {
        header('Location: ' . $url);
    }
}
